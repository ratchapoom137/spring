package dv.spring.kotlin.dao

import dv.spring.kotlin.entity.Manufacturer
import dv.spring.kotlin.repository.ManufacturerRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository

@Profile("db")
@Repository
class ManufacturerDaoDBImpl: ManufacturerDao{
    override fun findById(manuId: Long): Manufacturer? {
        return manufacturerRepository.findById(manuId).orElse(null)
    }

    override fun getManufacturers(): List<Manufacturer> {
        return manufacturerRepository.findAll().filterIsInstance(Manufacturer::class.java)
    }

    override fun save(manufacturer: Manufacturer): Manufacturer {
        return manufacturerRepository.save(manufacturer)
    }

    @Autowired
    lateinit var manufacturerRepository: ManufacturerRepository
}