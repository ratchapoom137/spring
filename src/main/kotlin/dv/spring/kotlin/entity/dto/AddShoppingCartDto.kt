package dv.spring.kotlin.entity.dto

data class AddShoppingCartDto(
        var customer: AddCustomerDto? = null,
        var selectedProduct: List<AddSelectedProductDto> = mutableListOf<AddSelectedProductDto>()
)

data class AddSelectedProductDto(
        var quantity: Int? = null,
        var products: AddProductDto? = null
)

data class AddProductDto(
        var id: Long? = null
)

data class AddCustomerDto(
        var id: Long? = null
)