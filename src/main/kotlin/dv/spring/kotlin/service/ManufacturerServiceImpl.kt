package dv.spring.kotlin.service

import dv.spring.kotlin.dao.ManufacturerDao
import dv.spring.kotlin.entity.Manufacturer
import dv.spring.kotlin.entity.dto.ManufacturerDto
import dv.spring.kotlin.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class ManufacturerServiceImpl : ManufacturerService {
    override fun getManufacturers(): List<Manufacturer> {
        return manufacturerDao.getManufacturers()
    }

    override fun save(manu: ManufacturerDto): Manufacturer {
        val manufacturer = MapperUtil.INSTANCE.mapManufacturer(manu)
        return manufacturerDao.save(manufacturer)
    }

    @Autowired
    lateinit var manufacturerDao: ManufacturerDao
}