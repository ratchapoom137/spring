package dv.spring.kotlin.repository

import dv.spring.kotlin.entity.Address
import org.springframework.data.repository.CrudRepository

interface AddressRepository: CrudRepository<Address,Long>