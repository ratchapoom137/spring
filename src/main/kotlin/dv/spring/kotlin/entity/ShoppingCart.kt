package dv.spring.kotlin.entity

import javax.persistence.*

@Entity
data class ShoppingCart(var shoppingCartStatus: ShoppingCartStatus? = null){
    @Id
    @GeneratedValue
    var id:Long? = null
    @OneToMany
    var selectedProducts = mutableListOf<SelectedProduct>()
    @OneToOne
    lateinit var customer: Customer
//    var customer: Customer? = null
    @OneToOne
    lateinit var shippingAddress: Address
//    var shippingAddress: Address? = null
}