package dv.spring.kotlin.service

import dv.spring.kotlin.dao.AddressDao
import dv.spring.kotlin.dao.CustomerDao
import dv.spring.kotlin.entity.Customer
import dv.spring.kotlin.entity.UserStatus
import dv.spring.kotlin.entity.dto.CustomerDto
import dv.spring.kotlin.util.MapperUtil
import org.mapstruct.Mapper
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.stereotype.Service
import javax.transaction.Transactional

@Service
class CustomerServiceImpl: CustomerService{
    @Transactional
    override fun remove(id: Long): Customer? {
        val customer = customerDao.findById(id)
        customer?.isDeleted = true
        return customer
    }

    override fun save(mapCustomerDto: Customer): Customer {
        val addresses = mapCustomerDto.defaultAddress?.let { addressDao.save(it) }
        val customers = customerDao.save(mapCustomerDto)
        customers.defaultAddress = addresses
        return customers
    }

    @Autowired
    lateinit var addressDao: AddressDao
    override fun save(addressId: Long, mapCustomerDto: Customer): Customer {
        val address = addressDao.findById(addressId)
        val customer = customerDao.save(mapCustomerDto)
        mapCustomerDto.defaultAddress = address
        return customer
    }

    override fun getCustomerByUserStatusWithPage(status: UserStatus, page: Int, pageSize: Int): Page<Customer> {
        return customerDao.getCustomerByUserStatusWithPage(status, page, pageSize)
    }

    override fun getCustomerByUserStatus(status: UserStatus): List<Customer> {
        return customerDao.getCustomerByUserStatus(status)
    }

    override fun getCustomerByProvinceName(name: String): List<Customer> {
        return customerDao.getProductByProvinceName(name)
    }

    override fun getCustomerByPartialNameAndEmail(name: String, email: String): List<Customer> {
        return customerDao.getProductByPartialNameAndEmail(name, email)
    }

    @Autowired
    lateinit var customerDao: CustomerDao
    override fun getCustomers(): List<Customer> {
        return customerDao.getCustomers()
    }

    override fun getCustomerByName(name: String): Customer?
        = customerDao.getCustomerByName(name)

    override fun getCustomerByPartialName(name: String): List<Customer> {
        return customerDao.getProductByPartialName(name)
    }
}