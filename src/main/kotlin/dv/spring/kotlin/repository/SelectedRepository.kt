package dv.spring.kotlin.repository

import dv.spring.kotlin.entity.SelectedProduct
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.repository.CrudRepository

interface SelectedRepository: CrudRepository<SelectedProduct,Long> {
    fun findByProducts_NameContainingIgnoreCase(name: String): List<SelectedProduct>
    fun findByProducts_NameContainingIgnoreCase(name: String, page: Pageable): Page<SelectedProduct>
}