package dv.spring.kotlin.entity.dto

data class PageProductDto(var totalPages: Int? = null,
                          var totalElement: Long? = null,
                          var products: List<ProductDto> = mutableListOf())