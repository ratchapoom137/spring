package dv.spring.kotlin.config

import dv.spring.kotlin.entity.*
import dv.spring.kotlin.repository.*
import dv.spring.kotlin.security.entity.Authority
import dv.spring.kotlin.security.entity.AuthorityName
import dv.spring.kotlin.security.entity.JwtUser
import dv.spring.kotlin.security.repository.AuthorityRepository
import dv.spring.kotlin.security.repository.UserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.ApplicationArguments
import org.springframework.boot.ApplicationRunner
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Component
import javax.transaction.Transactional

@Component
class ApplicationLoader: ApplicationRunner{
    @Autowired
    lateinit var manufacturerRepository: ManufacturerRepository
    @Autowired
    lateinit var productRepository: ProductRepository
    @Autowired
    lateinit var customerRepository: CustomerRepository
    @Autowired
    lateinit var addressRepository: AddressRepository
    @Autowired
    lateinit var shoppingRepository: ShoppingCartRepository
    @Autowired
    lateinit var selectedRepository: SelectedRepository
    @Autowired
    lateinit var dataLoader: DataLoader
    @Autowired
    lateinit var authorityRepository: AuthorityRepository
    @Autowired
    lateinit var userRepository: UserRepository

    fun loadUsernameAndPassword() {
        val auth1 = Authority(name = AuthorityName.ROLE_ADMIN)
        val auth2 = Authority(name = AuthorityName.ROLE_CUSTOMER)
        val auth3 = Authority(name = AuthorityName.ROLE_GENERAL)
        authorityRepository.save(auth1)
        authorityRepository.save(auth2)
        authorityRepository.save(auth3)
        val encoder = BCryptPasswordEncoder()
        val custl = Customer(name = "สมชาติ", email = "a@b.com")
        val custJwt = JwtUser(
                username = "customer",
                password = encoder.encode("password"),
                email = custl.email,
                enabled = true,
                firstname = custl.name,
                lastname = "unknown"
        )
        customerRepository.save(custl)
        userRepository.save(custJwt)
        custl.jwtUser = custJwt
        custJwt.user = custl
        custJwt.authorities.add(auth1)
        custJwt.authorities.add(auth2)
        custJwt.authorities.add(auth3)
    }

    @Transactional
    override fun run(args: ApplicationArguments?) {
        var manul = manufacturerRepository.save(Manufacturer("CAMT", "0000000"))
        var manu2 = manufacturerRepository.save(Manufacturer("SAMSUNG", "555666777888"))
        var manu3 = manufacturerRepository.save(Manufacturer("Apple", "053123456"))
        var product1 = productRepository.save(Product("CAMT", "The best College in CMU", 0.0, 1, "http://www.camt.cmu.ac.th/th/images/logo.jpg"))
        var product2 = productRepository.save(Product("iPhone", "It's a phone", 28000.0, 20, "https://www.jaymartstore.com/Products/iPhone-X-64GB-Space-Grey--1140900010552--4724"))
        var product3 = productRepository.save(Product("Prayuth", "The best PM ever", 1.0, 1, "https://upload.wikimedia.org/wikipedia/commons/thumb/9/9a/Prayut_Chan-o-cha_%28cropped%29_2016.jpg/200px-Prayut_Chan-o-cha_%28cropped%29_2016.jpg"))
        var product4 = productRepository.save(Product("Note 9", "Other Iphone", 28001.0, 10, "http://dynamic-cdn.eggdigital.com/e56zBiUt1.jpg"))
        manul.products.add(product1)
        product1.manufacturer = manul
        manu2.products.add(product4)
        product4.manufacturer = manu2
        manu3.products.add(product2)
        product2.manufacturer = manu3
        manul.products.add(product3)
        product3.manufacturer = manul

        var customer1 = customerRepository.save(Customer("Lung", "pm@go.th", UserStatus.ACTIVE))
        var address1 = addressRepository.save(Address("ถนนอนุสาวรีย์ประชาธิปไตย", "แขวง ดินสอ", "เขตดุสิต", "กรุงเทพ", "10123"))

        var customer2 = customerRepository.save(Customer("ชัชชาติ", "chut@taopoon.com", UserStatus.ACTIVE))
        var address2 = addressRepository.save(Address("239 มหาวิทยาลัยเชียงใหม่", "ต.สุเทพ", "อ.เมือง", "จ.เชียงใหม่", "50200"))

        var customer3 = customerRepository.save(Customer("ธนาธร", "thanathorn@life.com", UserStatus.PENDING))
        var address3 = addressRepository.save(Address("ซักที่บนโลก", "ต.สุขสันต์", "อ.ในเมือง", "จ.ขอนแก่น", "12457"))

        var selectedProduct1 = selectedRepository.save(SelectedProduct(4))
        var selectedProduct2 = selectedRepository.save(SelectedProduct(1))
        var selectedProduct3 = selectedRepository.save(SelectedProduct(1))
        var selectedProduct4 = selectedRepository.save(SelectedProduct(1))
        var selectedProduct5 = selectedRepository.save(SelectedProduct(2))
        var shoppingCart1 = shoppingRepository.save(ShoppingCart(ShoppingCartStatus.SENT))
        var shoppingCart2 = shoppingRepository.save(ShoppingCart(ShoppingCartStatus.WAIT))

        selectedProduct1.products=(product2)
        selectedProduct2.products=(product3)
        selectedProduct3.products=(product3)
        selectedProduct4.products=(product1)
        selectedProduct5.products=(product4)

        shoppingCart1.customer = (customer1)
        shoppingCart2.customer = (customer2)

        shoppingCart1.selectedProducts.add(selectedProduct1)
        shoppingCart1.selectedProducts.add(selectedProduct2)
        shoppingCart2.selectedProducts.add(selectedProduct3)
        shoppingCart2.selectedProducts.add(selectedProduct4)
        shoppingCart2.selectedProducts.add(selectedProduct5)

        customer1.defaultAddress = (address1)
        customer2.defaultAddress = (address2)
        customer3.defaultAddress = (address3)

        dataLoader.loadData()
        loadUsernameAndPassword()
    }
}