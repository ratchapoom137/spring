package dv.spring.kotlin.entity.dto

import dv.spring.kotlin.entity.ShoppingCartStatus

data class ShoppingCartDto(
        var shoppingCartStatus: ShoppingCartStatus? = null,
        var selectedProduct: List<SelectedProductDto>? = mutableListOf<SelectedProductDto>(),
        var customer: CustomerDto? = null
)