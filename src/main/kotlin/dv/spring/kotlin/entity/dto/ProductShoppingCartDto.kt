package dv.spring.kotlin.entity.dto

data class ProductShoppingCartDto(var name: String? = null,
                      var description: String? = null)