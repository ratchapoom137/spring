package dv.spring.kotlin.controller

import dv.spring.kotlin.entity.SelectedProduct
import dv.spring.kotlin.entity.dto.PageSelectedProductDto
import dv.spring.kotlin.entity.dto.SelectedProductDto
import dv.spring.kotlin.service.SelectedProductService
import dv.spring.kotlin.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.http.ResponseEntity.ok
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
class SelectedProductController {
    @Autowired
    lateinit var selectedProductService: SelectedProductService

    @GetMapping("/selectedProduct")
    fun getSelectedProduct(): ResponseEntity<Any> {
        val selectedProducts = selectedProductService.getSelectedProduct()
        return ok(MapperUtil.INSTANCE.mapSelectedProductDto(selectedProducts))
    }

    @GetMapping("/selectedProduct/productName")
    fun getSelectedProductByProductName(@RequestParam("name") name: String): ResponseEntity<Any>{
        val output: List<SelectedProductDto> = MapperUtil.INSTANCE.mapSelectedProductDto(
                selectedProductService.getSelectedProductByProductName(name)
        )
        return ResponseEntity.ok(output)
    }

    @GetMapping("/selectedProduct/productNamePageable")
    fun getSelectedProductByProductNameWithPage(@RequestParam("name") name: String,
                                                @RequestParam("page") page: Int,
                                                @RequestParam("pageSize") pageSize: Int) : ResponseEntity<Any>{
        val output = selectedProductService.getSelectedProductByProductNameWithPage(name, page, pageSize)
        return ResponseEntity.ok(PageSelectedProductDto(totalPages = output.totalPages,
                totalElements = output.totalElements,
                selectedProducts = MapperUtil.INSTANCE.mapSelectedProductDto(output.content)))
        return ResponseEntity.ok(output)
    }
}