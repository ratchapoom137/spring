package dv.spring.kotlin.entity

import javax.persistence.*

@Entity
data class Manufacturer(var name: String? = null, var telNo: String? = null) {
    @Id
    @GeneratedValue
    var id:Long? = null
    @OneToMany(mappedBy = "manufacturer")
    var products = mutableListOf<Product>()
    @OneToOne
    lateinit var Address : Address
//    var Address : Address? = null
}