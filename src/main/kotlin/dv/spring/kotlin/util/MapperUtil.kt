package dv.spring.kotlin.util

import dv.spring.kotlin.entity.*
import dv.spring.kotlin.entity.dto.*
import dv.spring.kotlin.security.entity.Authority
import org.mapstruct.*
import org.mapstruct.factory.Mappers

@Mapper (componentModel = "spring")
interface MapperUtil{
    companion object {
        val INSTANCE = Mappers.getMapper(MapperUtil::class.java)
    }

    @Mappings(
            Mapping(source = "manufacturer", target = "manu")
    )
    fun mapProductDto(product: Product?): ProductDto?

    fun mapProductDto(product: List<Product>): List<ProductDto>

    fun mapManufacturer(manu: Manufacturer): ManufacturerDto

    fun mapManufacturer(manu: List<Manufacturer>): List<ManufacturerDto>

    @Mappings(
            Mapping(source = "customer.jwtUser.username", target = "username"),
            Mapping(source = "customer.jwtUser.authorities", target = "authorities")
    )
    fun mapUser(customer: Customer): UserDto

    fun mapCustomerShoppingCartDto(customer: Customer?): CustomerShoppingCartDto

    fun mapCustomerDto(customer: Customer?): CustomerDto

    fun mapCustomerDto(customer: List<Customer>): List<CustomerDto>

    @Mappings(
            Mapping(source = "selectedProducts", target = "selectedProduct")
    )
    fun mapShoppingCartDto(shoppingCart: ShoppingCart): ShoppingCartDto

    fun mapShoppingCartDto(shoppingCart: List<ShoppingCart>): List<ShoppingCartDto>

    fun mapSelectedProductDto(selectedProducts: SelectedProduct): SelectedProductDto

    fun mapSelectedProductDto(selectedProducts: List<SelectedProduct>): List<SelectedProductDto>

    fun mapShoppingCartByCustomerDto(shoppingCart: ShoppingCart): ShoppingCartByCustomerDto

    fun mapShoppingCartByCustomerDto(shoppingCart: List<ShoppingCart>): List<ShoppingCartByCustomerDto>

    fun mapShoppingCartCustomerByProductName(shoppingCart: ShoppingCart): CustomerByProductNameDto

    fun mapShoppingCartCustomerByProductName(shoppingCart: List<ShoppingCart>): List<CustomerByProductNameDto>

    @InheritInverseConfiguration
    fun mapManufacturer(manu: ManufacturerDto): Manufacturer

    fun mapAddressDto(address: Address): AddressDto

    fun mapAddressDto(address: List<Address>): List<AddressDto>

    @InheritInverseConfiguration
    fun mapAddressDto(address: AddressDto): Address

    @InheritInverseConfiguration
    fun mapProductDto(productDto: ProductDto): Product

    @InheritInverseConfiguration
    fun mapCustomerDto(customerDto: CustomerDto): Customer

    @InheritInverseConfiguration
    fun mapShoppingCartDto(addShoppingCartDto: AddShoppingCartDto): ShoppingCart

    fun mapAuthority(authority: AuthorityDto): AuthorityDto
    fun mapAuthority(authority: List<Authority>): List<AuthorityDto>
}
