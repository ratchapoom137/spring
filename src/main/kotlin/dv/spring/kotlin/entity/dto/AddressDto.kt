package dv.spring.kotlin.entity.dto

data class AddressDto (
        var homeAddess: String? = null,
        var subdistrict: String? = null,
        var district: String? = null,
        var province: String? = null,
        var postCode: String? = null,
        var id: Long? = null
)