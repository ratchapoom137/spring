package dv.spring.kotlin.entity.dto

data class ShoppingCartByCustomerDto(
        var customer: CustomerShoppingCartDto? = null,
        var selectedProducts: List<SelectedProductByCustomerDto>? = mutableListOf<SelectedProductByCustomerDto>()
)