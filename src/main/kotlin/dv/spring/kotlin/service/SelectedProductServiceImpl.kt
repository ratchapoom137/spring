package dv.spring.kotlin.service

import dv.spring.kotlin.dao.SelectedProductDao
import dv.spring.kotlin.entity.SelectedProduct
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.stereotype.Service

@Service
class SelectedProductServiceImpl: SelectedProductService{
    override fun getSelectedProductByProductNameWithPage(name: String, page: Int, pageSize: Int): Page<SelectedProduct> {
        return selectedProductDao.getSelectedProductByProductNameWithPage(name, page, pageSize)
    }

    override fun getSelectedProductByProductName(name: String): List<SelectedProduct> {
        return selectedProductDao.getSelectedProductByProductName(name)
    }

    @Autowired
    lateinit var selectedProductDao: SelectedProductDao
    override fun getSelectedProduct(): List<SelectedProduct> {
        return selectedProductDao.getSelectedProduct()
    }
}