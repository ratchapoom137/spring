package dv.spring.kotlin.service

import dv.spring.kotlin.entity.Manufacturer
import dv.spring.kotlin.entity.dto.ManufacturerDto

interface ManufacturerService {
    fun save(manu: ManufacturerDto): Manufacturer
    fun getManufacturers(): List<Manufacturer>
}