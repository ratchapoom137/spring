package dv.spring.kotlin.service

import dv.spring.kotlin.entity.SelectedProduct
import org.springframework.data.domain.Page

interface SelectedProductService{
    fun getSelectedProduct(): List<SelectedProduct>
    fun getSelectedProductByProductName(name: String): List<SelectedProduct>
    fun getSelectedProductByProductNameWithPage(name: String, page: Int, pageSize: Int): Page<SelectedProduct>
}
