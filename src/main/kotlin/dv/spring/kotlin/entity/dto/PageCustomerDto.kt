package dv.spring.kotlin.entity.dto

data class PageCustomerDto(var totalPages: Int? = null,
                          var totalElement: Long? = null,
                          var customer: List<CustomerDto> = mutableListOf())