package dv.spring.kotlin.entity.dto

import dv.spring.kotlin.entity.Address

data class CustomerShoppingCartDto(
        var name:String? = null,
        var  defaultAddress: Address? = null
)