package dv.spring.kotlin.repository

import dv.spring.kotlin.entity.ShoppingCart
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.repository.CrudRepository

interface ShoppingCartRepository: CrudRepository<ShoppingCart,Long> {
    fun findByCustomer_NameContainingIgnoreCase(name: String): List<ShoppingCart>
    fun findByCustomer_NameContainingIgnoreCase(name: String, page: Pageable): Page<ShoppingCart>
    fun findAll(page: Pageable): Page<ShoppingCart>
    fun findBySelectedProducts_Products_NameContainingIgnoreCase(name: String): List<ShoppingCart>
    fun findBySelectedProducts_Products_NameContainingIgnoreCase(name: String, page: Pageable): Page<ShoppingCart>
}