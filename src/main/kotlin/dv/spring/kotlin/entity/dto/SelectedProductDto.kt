package dv.spring.kotlin.entity.dto

data class SelectedProductDto(
        var quantity: Int? = null,
        var products: ProductDto? = null
)