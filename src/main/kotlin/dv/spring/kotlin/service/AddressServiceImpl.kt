package dv.spring.kotlin.service

import dv.spring.kotlin.dao.AddressDao
import dv.spring.kotlin.entity.Address
import dv.spring.kotlin.entity.dto.AddressDto
import dv.spring.kotlin.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class AddressServiceImpl: AddressService {
    override fun save(address: AddressDto): Address {
        val addresses = MapperUtil.INSTANCE.mapAddressDto(address)
        return addressDao.save(addresses)
    }

    override fun getAddresses(): List<Address> {
        return addressDao.getAddresses()
    }

    @Autowired
    lateinit var addressDao: AddressDao
}