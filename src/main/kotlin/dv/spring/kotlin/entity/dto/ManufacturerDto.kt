package dv.spring.kotlin.entity.dto

data class ManufacturerDto(
        var name:String? = null,
        var telNo:String? = null,
        var id:Long? = null
)