package dv.spring.kotlin.entity.dto

data class SelectedProductByCustomerDto(
        var products: ProductShoppingCartDto? = null,
        var quantity: Int? = null
)