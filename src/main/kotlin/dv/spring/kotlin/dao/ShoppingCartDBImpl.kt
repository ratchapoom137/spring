package dv.spring.kotlin.dao

import dv.spring.kotlin.entity.Customer
import dv.spring.kotlin.entity.ShoppingCart
import dv.spring.kotlin.repository.ShoppingCartRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.stereotype.Repository

@Profile("db")
@Repository
class ShoppingCartDBImpl: ShoppingCartDao{
    override fun save(shoppingCarts: ShoppingCart): ShoppingCart {
        return shoppingCartRepository.save(shoppingCarts)
    }

    override fun getCustomerByProductNameWithPage(name: String, page: Int, pageSize: Int): Page<ShoppingCart> {
        return shoppingCartRepository.findBySelectedProducts_Products_NameContainingIgnoreCase(name, PageRequest.of(page, pageSize))
    }

    override fun getCustomerByProductName(name: String): List<ShoppingCart> {
        return shoppingCartRepository.findBySelectedProducts_Products_NameContainingIgnoreCase(name)
    }

    override fun getShoppingCartsByProductPartialNameWithPage(name: String, page: Int, pageSize: Int): Page<ShoppingCart> {
        return shoppingCartRepository.findBySelectedProducts_Products_NameContainingIgnoreCase(name, PageRequest.of(page, pageSize))
    }

    override fun getShoppingCartsByProductPartialName(name: String): List<ShoppingCart> {
        return shoppingCartRepository.findBySelectedProducts_Products_NameContainingIgnoreCase(name)
    }

    override fun getShoppingCartsByCustomerName2WithPage(page: Int, pageSize: Int): Page<ShoppingCart> {
        return shoppingCartRepository.findAll(PageRequest.of(page, pageSize))
    }

    override fun getShoppingCartsByCustomerNameWithPage(name: String, page: Int, pageSize: Int): Page<ShoppingCart> {
        return shoppingCartRepository.findByCustomer_NameContainingIgnoreCase(name, PageRequest.of(page, pageSize))
    }

    override fun getShoppingCartsByCustomerName(name: String): List<ShoppingCart> {
        return shoppingCartRepository.findByCustomer_NameContainingIgnoreCase(name)
    }

    @Autowired
    lateinit var shoppingCartRepository: ShoppingCartRepository

    override fun getShoppingCarts(): List<ShoppingCart> {
        return shoppingCartRepository.findAll().filterIsInstance(ShoppingCart::class.java)
    }
}