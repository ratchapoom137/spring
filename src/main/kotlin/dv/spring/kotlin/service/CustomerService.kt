package dv.spring.kotlin.service

import dv.spring.kotlin.entity.Customer
import dv.spring.kotlin.entity.UserStatus
import dv.spring.kotlin.entity.dto.CustomerDto
import org.springframework.data.domain.Page

interface CustomerService {
    fun getCustomers(): List<Customer>
    fun getCustomerByName(name: String): Customer?
    fun getCustomerByPartialName(name: String): List<Customer>
    fun getCustomerByPartialNameAndEmail(name: String, email: String): List<Customer>
    fun getCustomerByProvinceName(name: String): List<Customer>
    fun getCustomerByUserStatus(status: UserStatus): List<Customer>
    fun getCustomerByUserStatusWithPage(status: UserStatus, page: Int, pageSize: Int): Page<Customer>
    fun save(addressId: Long, mapCustomerDto: Customer): Customer
    fun save(mapCustomerDto: Customer): Customer
    fun remove(id: Long): Customer?
}