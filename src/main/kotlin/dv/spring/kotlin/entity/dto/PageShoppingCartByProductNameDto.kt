package dv.spring.kotlin.entity.dto

data class PageShoppingCartByProductNameDto(var totalPages: Int? = null,
                               var totalElements: Long? = null,
                               var selectedProducts: List<ShoppingCartDto> = mutableListOf())