package dv.spring.kotlin.entity

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id

@Entity
data class Admin(
        override var name: String? = null,
        override var email: String? = null,
        override var userStatus: UserStatus? = UserStatus.ACTIVE)
    : User(name, email, userStatus) {

}