package dv.spring.kotlin.service

import dv.spring.kotlin.entity.Address
import dv.spring.kotlin.entity.dto.AddressDto

interface AddressService {
    fun getAddresses(): List<Address>
    fun save(address: AddressDto): Address
}