package dv.spring.kotlin.entity.dto

data class PageSelectedProductDto(var totalPages: Int? = null,
                                  var totalElements: Long? = null,
                                  var selectedProducts: List<SelectedProductDto> = mutableListOf())