package dv.spring.kotlin.controller

import dv.spring.kotlin.entity.UserStatus
import dv.spring.kotlin.entity.dto.*
import dv.spring.kotlin.service.CustomerService
import dv.spring.kotlin.service.ShoppingCartService
import dv.spring.kotlin.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
class CustomerController {
    @Autowired
    lateinit var customerService: CustomerService
    @Autowired
    lateinit var shoppingCartService: ShoppingCartService

    @GetMapping("/customer")
    fun getCustomers(): ResponseEntity<Any> {
        val customers = customerService.getCustomers()
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapCustomerDto(customers))
    }

    @PostMapping("/customer/address/{addressId}")
    fun addCustomer(@RequestBody customerDto: CustomerDto, @PathVariable addressId: Long): ResponseEntity<Any> {
        val output = customerService.save(addressId, MapperUtil.INSTANCE.mapCustomerDto(customerDto))
        val outputDto = MapperUtil.INSTANCE.mapCustomerDto(output)
        outputDto?.let { return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
    }

    @PostMapping("/customer/address")
    fun addCustomerNewAddress(@RequestBody customerDto: CustomerDto): ResponseEntity<Any> {
        val output = customerService.save(MapperUtil.INSTANCE.mapCustomerDto(customerDto))
        val outputDto = MapperUtil.INSTANCE.mapCustomerDto(output)
        outputDto?.let{ return ResponseEntity.ok(it)}
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
    }

    @GetMapping("/customer/query")
    fun getCustomers(@RequestParam("name") name: String): ResponseEntity<Any> {
        var output = MapperUtil.INSTANCE.mapCustomerDto(customerService.getCustomerByName(name))
        output?.let { return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
    }

    @GetMapping("/customer/partialCustomer")
    fun getCustomerPartialName(@RequestParam("name") name: String,
                               @RequestParam(value = "email", required = false) email: String?): ResponseEntity<Any> {
        val output: List<CustomerDto> = MapperUtil.INSTANCE.mapCustomerDto(
                customerService.getCustomerByPartialNameAndEmail(name, name)
        )
        return ResponseEntity.ok(output)
    }

    @GetMapping("/customer/provinceName")
    fun getCustomerByProvinceName(@RequestParam("name") name: String): ResponseEntity<Any> {
        val output: List<CustomerDto> = MapperUtil.INSTANCE.mapCustomerDto(
                customerService.getCustomerByProvinceName(name)
        )
        return ResponseEntity.ok(output)
    }

    @GetMapping("/customer/userStatus")
    fun getCustomerByUserStatus(@RequestParam("status") status: UserStatus): ResponseEntity<Any> {
        val output: List<CustomerDto> = MapperUtil.INSTANCE.mapCustomerDto(
                customerService.getCustomerByUserStatus(status)
        )
        return ResponseEntity.ok(output)
    }

    @GetMapping("/customer/userStatus/page")
    fun getCustomerByUserStatusWithPage(@RequestParam("status") status: UserStatus,
                                        @RequestParam("page") page: Int,
                                        @RequestParam("pageSize") pageSize: Int): ResponseEntity<Any> {
        val output = customerService.getCustomerByUserStatusWithPage(status, page, pageSize)
        return ResponseEntity.ok(PageCustomerDto(totalPages = output.totalPages,
                totalElement = output.totalElements,
                customer = MapperUtil.INSTANCE.mapCustomerDto(output.content)))
    }

    @GetMapping("/customer/productName")
    fun getCustomerByProductName(@RequestParam("name") name: String): ResponseEntity<Any> {
        val output = MapperUtil.INSTANCE.mapShoppingCartCustomerByProductName(
                shoppingCartService.getCustomerByProductName(name)
        )
        return ResponseEntity.ok(output)
    }

    @GetMapping("/customer/productName/page")
    fun getCustomerByProductNameWithPage(@RequestParam("name") name: String,
                                         @RequestParam("page") page: Int,
                                         @RequestParam("pageSize") pageSize: Int): ResponseEntity<Any> {
        val output = shoppingCartService.getCustomerByProductNameWithPage(name, page, pageSize)
        return ResponseEntity.ok(PageCustomerByProductNameDto(totalPages = output.totalPages,
                totalElement = output.totalElements,
                customer = MapperUtil.INSTANCE.mapShoppingCartCustomerByProductName(output.content)))
    }

    @DeleteMapping("/customer/{id}")
    fun deleteCustomer(@PathVariable("id") id: Long): ResponseEntity<Any>{
        val customer = customerService.remove(id)
        val outputCustomer = MapperUtil.INSTANCE.mapCustomerDto(customer)
        outputCustomer?.let { return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("the product id is not found")
    }
}