package dv.spring.kotlin.dao

import dv.spring.kotlin.entity.SelectedProduct
import dv.spring.kotlin.entity.ShoppingCart
import org.springframework.data.domain.Page

interface ShoppingCartDao{
    fun getShoppingCarts(): List<ShoppingCart>
    fun getShoppingCartsByCustomerName(name: String): List<ShoppingCart>
    fun getShoppingCartsByCustomerNameWithPage(name: String, page: Int, pageSize: Int): Page<ShoppingCart>
    fun getShoppingCartsByCustomerName2WithPage(page: Int, pageSize: Int): Page<ShoppingCart>
    fun getShoppingCartsByProductPartialName(name: String): List<ShoppingCart>
    fun getShoppingCartsByProductPartialNameWithPage(name: String, page: Int, pageSize: Int): Page<ShoppingCart>
    fun getCustomerByProductName(name: String): List<ShoppingCart>
    fun getCustomerByProductNameWithPage(name: String, page: Int, pageSize: Int): Page<ShoppingCart>
     fun save(shoppingCarts: ShoppingCart): ShoppingCart
}