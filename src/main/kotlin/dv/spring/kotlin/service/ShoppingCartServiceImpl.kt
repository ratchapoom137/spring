package dv.spring.kotlin.service

import dv.spring.kotlin.dao.CustomerDao
import dv.spring.kotlin.dao.ProductDao
import dv.spring.kotlin.dao.SelectedProductDao
import dv.spring.kotlin.dao.ShoppingCartDao
import dv.spring.kotlin.entity.Customer
import dv.spring.kotlin.entity.SelectedProduct
import dv.spring.kotlin.entity.ShoppingCart
import dv.spring.kotlin.entity.dto.AddShoppingCartDto
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.stereotype.Service
import javax.transaction.Transactional

@Service
class ShoppingCartServiceImpl: ShoppingCartService{
    @Autowired
    lateinit var selectedProductDao: SelectedProductDao
    @Autowired
    lateinit var customerDao: CustomerDao
    @Autowired
    lateinit var productDao: ProductDao
    @Transactional
    override fun save(mapShoppingCartDto: AddShoppingCartDto): ShoppingCart {
        val selected = mutableListOf<SelectedProduct>()
        for (item in mapShoppingCartDto.selectedProduct) {
            var product = productDao.findById(item.products!!.id!!)
            selected.add(selectedProductDao.save(item.quantity!!, product))
        }
        val customer = customerDao.findById(mapShoppingCartDto.customer!!.id!!)
        val shoppingCarts = ShoppingCart()
        shoppingCarts.customer = customer!!
        shoppingCarts.selectedProducts = selected!!
        shoppingCartDao.save(shoppingCarts)
        return shoppingCarts
    }

    override fun getCustomerByProductNameWithPage(name: String, page: Int, pageSize: Int): Page<ShoppingCart> {
        return shoppingCartDao.getCustomerByProductNameWithPage(name, page, pageSize)
    }

    override fun getCustomerByProductName(name: String): List<ShoppingCart> {
        return shoppingCartDao.getCustomerByProductName(name)
    }

    override fun getShoppingCartsByProductPartialNameWithPage(name: String, page: Int, pageSize: Int): Page<ShoppingCart> {
        return shoppingCartDao.getShoppingCartsByProductPartialNameWithPage(name, page, pageSize)
    }

    override fun getShoppingCartsByProductPartialName(name: String): List<ShoppingCart> {
        return shoppingCartDao.getShoppingCartsByProductPartialName(name)
    }

    override fun getShoppingCartsByCustomerName2WithPage(page: Int, pageSize: Int): Page<ShoppingCart> {
        return shoppingCartDao.getShoppingCartsByCustomerName2WithPage(page, pageSize)
    }

    override fun getShoppingCartsByCustomerNameWithPage(name: String, page: Int, pageSize: Int): Page<ShoppingCart> {
        return shoppingCartDao.getShoppingCartsByCustomerNameWithPage(name, page, pageSize)
    }

    override fun getShoppingCartsByCustomerName(name: String): List<ShoppingCart> {
        return shoppingCartDao.getShoppingCartsByCustomerName(name)
    }

    @Autowired
    lateinit var shoppingCartDao: ShoppingCartDao
    override fun getShoppingCarts(): List<ShoppingCart> {
        return shoppingCartDao.getShoppingCarts()
    }

}