package dv.spring.kotlin.entity

import dv.spring.kotlin.security.entity.JwtUser
import javax.persistence.*

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
abstract class User(
        open var name: String?,
        open var email: String?,
        open var userStatus: UserStatus? = null
) {
    @Id
    @GeneratedValue
    var id: Long? = null

    @OneToOne
    var jwtUser: JwtUser? = null
}