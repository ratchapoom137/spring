package dv.spring.kotlin.dao

import dv.spring.kotlin.entity.Product
import dv.spring.kotlin.entity.SelectedProduct
import org.springframework.data.domain.Page

interface SelectedProductDao{
    fun getSelectedProduct(): List<SelectedProduct>
    fun getSelectedProductByProductName(name: String): List<SelectedProduct>
    fun getSelectedProductByProductNameWithPage(name: String, page: Int, pageSize: Int): Page<SelectedProduct>
    fun save(quantity: Int, product: Product?): SelectedProduct
}