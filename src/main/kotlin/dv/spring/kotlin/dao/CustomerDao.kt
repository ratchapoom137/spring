package dv.spring.kotlin.dao

import dv.spring.kotlin.entity.Customer
import dv.spring.kotlin.entity.UserStatus
import dv.spring.kotlin.entity.dto.CustomerDto
import org.springframework.data.domain.Page

interface CustomerDao {
    fun getCustomers(): List<Customer>
    fun getCustomerByName(name: String): Customer?
    fun getProductByPartialName(name: String): List<Customer>
    fun getProductByPartialNameAndEmail(name: String, email: String): List<Customer>
    fun getProductByProvinceName(name: String): List<Customer>
    fun getCustomerByUserStatus(status: UserStatus): List<Customer>
    fun getCustomerByUserStatusWithPage(status: UserStatus, page: Int, pageSize: Int): Page<Customer>
    fun save(mapCustomerDto: Customer): Customer
    fun findById(id: Long): Customer?
}