package dv.spring.kotlin.dao

import dv.spring.kotlin.entity.Customer
import dv.spring.kotlin.entity.UserStatus
import dv.spring.kotlin.entity.dto.CustomerDto
import dv.spring.kotlin.repository.CustomerRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.stereotype.Repository

@Profile("db")
@Repository
class CustomerDaoDBImpl: CustomerDao{
    override fun findById(id: Long): Customer? {
        return customerRepository.findById(id).orElse(null)
    }

    override fun save(mapCustomerDto: Customer): Customer {
        return customerRepository.save(mapCustomerDto)
    }

    override fun getCustomerByUserStatusWithPage(status: UserStatus, page: Int, pageSize: Int): Page<Customer> {
        return customerRepository.findByUserStatus(status, PageRequest.of(page, pageSize))
    }

    override fun getCustomerByUserStatus(status: UserStatus): List<Customer> {
        return customerRepository.findByUserStatus(status)
    }

    override fun getProductByProvinceName(name: String): List<Customer> {
        return customerRepository.findByDefaultAddress_ProvinceContainingIgnoreCase(name)
    }

    @Autowired
    lateinit var customerRepository: CustomerRepository

    override fun getCustomers(): List<Customer> {
//        return customerRepository.findAll().filterIsInstance(Customer::class.java)
        return customerRepository.findByIsDeletedIsFalse()
    }

    override fun getCustomerByName(name: String): Customer? {
        return customerRepository.findByName(name)
    }

    override fun getProductByPartialName(name: String): List<Customer> {
        return customerRepository.findByNameEndingWith(name)
    }

    override fun getProductByPartialNameAndEmail(name: String, email: String): List<Customer> {
        return customerRepository.findByNameContainingIgnoreCaseOrEmailContainingIgnoreCase(name, email)
    }
}