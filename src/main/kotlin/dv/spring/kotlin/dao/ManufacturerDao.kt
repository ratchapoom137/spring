package dv.spring.kotlin.dao

import dv.spring.kotlin.entity.Manufacturer

interface ManufacturerDao{
    fun save(manufacturer: Manufacturer): Manufacturer
    fun getManufacturers(): List<Manufacturer>
    fun findById(manuId: Long): Manufacturer?
}