package dv.spring.kotlin.entity.dto

data class PageCustomerByProductNameDto(var totalPages: Int? = null,
                           var totalElement: Long? = null,
                           var customer: List<CustomerByProductNameDto> = mutableListOf())