//package dv.spring.kotlin.controller
//
//import dv.spring.kotlin.entity.Product
//import org.springframework.http.ResponseEntity
//import org.springframework.web.bind.annotation.*
//
//@RestController
//class assessment {
//    @GetMapping("/getAppName")
//    fun getAppName(): String {
//        return "assessment"
//    }
//
//    @GetMapping("/product")
//    fun getProduct(): ResponseEntity<Any> {
//        val product = Product("iPhone", "A new telephone", 28000.00, 5)
//        return ResponseEntity.ok(product)
//    }
//
//    @GetMapping("/product/{name}")
//    fun getPathProduct(@PathVariable("name") name: String): ResponseEntity<Any> {
//        val product = Product("iPhone", "A new telephone", 28000.00, 5)
//        if (product.name.equals(name)) {
//            return ResponseEntity.ok(product)
//        } else {
//            return ResponseEntity.notFound().build()
//        }
//    }
//
//    @PostMapping("/setZeroQuantity")
//    fun resetQuantity(@RequestBody product: Product): ResponseEntity<Any> {
//        product.quantity = 0
//        return ResponseEntity.ok(product)
//    }
//
//    @PostMapping("/totalPrice")
//    fun getTotalPrice(@RequestBody product: Array<Product>): String {
//        var total = 0.00
//        for (value in product) {
//            total = total + value.price
//        }
//        return "$total"
//    }
//
//    @PostMapping("/avaliableProduct")
//    fun getAvailableProduct(@RequestBody product: Array<Product>): ResponseEntity<Any> {
//        var output = mutableListOf<Product>()
//        for (value in product) {
//            if (value.quantity != 0) {
//                output.add(value)
//            }
//        }
//        return ResponseEntity.ok(output)
//    }
//}