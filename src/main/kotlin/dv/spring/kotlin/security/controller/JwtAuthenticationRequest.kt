package dv.spring.kotlin.security.controller

data class JwtAuthenticationRequest(var username: String? = null,
                                    var password: String? = null)
