package dv.spring.kotlin.entity

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.OneToOne

@Entity
data class SelectedProduct(var quantity: Int) {
        @Id
    @GeneratedValue
    var id:Long? = null
    @OneToOne
    lateinit var products: Product
//    var product : Product? = null
}