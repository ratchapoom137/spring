package dv.spring.kotlin.service

import dv.spring.kotlin.entity.ShoppingCart
import dv.spring.kotlin.entity.dto.AddShoppingCartDto
import org.springframework.data.domain.Page

interface ShoppingCartService{
    fun getShoppingCarts(): List<ShoppingCart>
    fun getShoppingCartsByCustomerName(name: String): List<ShoppingCart>
    fun getShoppingCartsByCustomerNameWithPage(name: String, page: Int, pageSize: Int): Page<ShoppingCart>
    fun getShoppingCartsByCustomerName2WithPage(page: Int, pageSize: Int): Page<ShoppingCart>
    fun getShoppingCartsByProductPartialName(name: String): List<ShoppingCart>
    fun getShoppingCartsByProductPartialNameWithPage(name: String, page: Int, pageSize: Int): Page<ShoppingCart>
    fun getCustomerByProductName(name: String): List<ShoppingCart>
    fun getCustomerByProductNameWithPage(name: String, page: Int, pageSize: Int): Page<ShoppingCart>
    fun save(mapShoppingCartDto: AddShoppingCartDto): ShoppingCart
}