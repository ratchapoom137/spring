package dv.spring.kotlin.dao

import dv.spring.kotlin.entity.Product
import dv.spring.kotlin.entity.SelectedProduct
import dv.spring.kotlin.repository.SelectedRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.stereotype.Repository
import javax.transaction.Transactional

@Profile("db")
@Repository
class SelectedProductDaoDBImpl: SelectedProductDao{
    @Transactional
    override fun save(quantity: Int, product: Product?): SelectedProduct {
        val result = selectedProductRepository.save(SelectedProduct(quantity))
        result.products = product !!
        return result
    }

    override fun getSelectedProductByProductNameWithPage(name: String, page: Int, pageSize: Int): Page<SelectedProduct> {
        return selectedProductRepository.findByProducts_NameContainingIgnoreCase(name, PageRequest.of(page, pageSize))
    }

    override fun getSelectedProductByProductName(name: String): List<SelectedProduct> {
        return selectedProductRepository.findByProducts_NameContainingIgnoreCase(name)
    }

    override fun getSelectedProduct(): List<SelectedProduct> {
        return selectedProductRepository.findAll().filterIsInstance(SelectedProduct::class.java)
    }

    @Autowired
    lateinit var selectedProductRepository: SelectedRepository

}
