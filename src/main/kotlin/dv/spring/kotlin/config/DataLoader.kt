package dv.spring.kotlin.config

import dv.spring.kotlin.entity.Manufacturer
import dv.spring.kotlin.entity.Product
import dv.spring.kotlin.repository.ManufacturerRepository
import dv.spring.kotlin.repository.ProductRepository
import org.apache.poi.ss.usermodel.CellType
import org.apache.poi.ss.usermodel.Row
import org.apache.poi.ss.util.CellReference
import org.apache.poi.ss.util.CellUtil
import org.apache.poi.xssf.usermodel.XSSFWorkbook
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.core.io.ClassPathResource
import org.springframework.stereotype.Component
import org.springframework.transaction.annotation.Transactional
import java.io.IOException

@Component
class DataLoader {
    @Value("\${xlsxPath}")
    val xlsxPath: String? = null
    @Autowired
    lateinit var manufacturerRepository: ManufacturerRepository
    @Autowired
    lateinit var productRepository: ProductRepository


    fun loadData() {
        createManufacturerData()
        createProductData()
    }

    fun getCellData(row: Row, col: String): String {
        val colIdx = CellReference.convertColStringToIndex(col)
        return getCellData(row, colIdx)
    }

    fun getCellData(row: Row, colIdx: Int): String {
        val cell = CellUtil.getCell(row, colIdx)
        if (cell.cellType == CellType.STRING) {
            if (cell.stringCellValue == "NULL")
                return ""
            return cell.stringCellValue
        } else if (cell.cellType == CellType.NUMERIC) {
            cell.cellType = CellType.STRING
            return cell.stringCellValue
        }
        return ""
    }

    @Transactional
    fun createManufacturerData() {
        try {
            val file = xlsxPath?.let { ClassPathResource(it).inputStream }
            val workbook = XSSFWorkbook(file)
            val sheet = workbook.getSheet("manufacturer")
            val rowIterator = sheet.iterator()
            rowIterator.next()
            while (rowIterator.hasNext()) {
                val row = rowIterator.next()
                if (getCellData(row, "A") != "") {
                    manufacturerRepository.save(Manufacturer(getCellData(row, "A"), getCellData(row, "B")))
                }
            }
        } catch (e: IOException) {

        }
    }
    @Transactional
    fun createProductData() {
        try {
            val file = xlsxPath?.let { ClassPathResource(it).inputStream }
            val workbook = XSSFWorkbook(file)
            val sheet = workbook.getSheet("product")
            val rowIterator = sheet.iterator()
            rowIterator.next()
            while (rowIterator.hasNext()) {
                val row = rowIterator.next()
                if (getCellData(row, "A") != "") {
                    productRepository.save(Product(name = getCellData(row, "A"),
                            description = getCellData(row, "B"),
                            imageUrl = getCellData(row, "C"),
                            price = getCellData(row, "D").toDouble(),
                            amountInStock = getCellData(row, "E").toInt(),
                            manufacturer = manufacturerRepository.findByName(getCellData(row, "F"))))
                }
            }
        } catch (e: IOException) {

        }
    }
}