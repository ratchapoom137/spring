package dv.spring.kotlin.entity.dto

data class CustomerByProductNameDto(
        var customer: CustomerDto? = null
)