package dv.spring.kotlin.controller

import dv.spring.kotlin.entity.dto.*
import dv.spring.kotlin.service.ShoppingCartService
import dv.spring.kotlin.util.MapperUtil
import org.mapstruct.Mapper
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.http.ResponseEntity.ok
import org.springframework.web.bind.annotation.*

@RestController
class ShoppingCartController {
    @Autowired
    lateinit var shoppingCartService: ShoppingCartService

    @GetMapping("/shoppingCart")
    fun getShoppingCarts(): ResponseEntity<Any> {
        val shoppingCarts = shoppingCartService.getShoppingCarts()
        return ok(MapperUtil.INSTANCE.mapShoppingCartDto(shoppingCarts))
    }

    @GetMapping("/shoppingCart/list")
    fun getShoppingCartByCustomerName(@RequestParam("name") name: String): ResponseEntity<Any> {
        val output: List<ShoppingCartByCustomerDto> = MapperUtil.INSTANCE.mapShoppingCartByCustomerDto(
                shoppingCartService.getShoppingCartsByCustomerName(name)
        )
        return ResponseEntity.ok(output)
    }

    @GetMapping("/shoppingCart/listPageable")
    fun getShoppingCartByCustomerName2Withpage(@RequestParam("page") page: Int,
                                               @RequestParam("pageSize") pageSize: Int): ResponseEntity<Any> {
        val output = shoppingCartService.getShoppingCartsByCustomerName2WithPage(page, pageSize)
        return ResponseEntity.ok(PageShoppingCartDto(totalPages = output.totalPages,
                totalElements = output.totalElements,
                selectedProducts = MapperUtil.INSTANCE.mapShoppingCartByCustomerDto(output.content)))
    }

    @GetMapping("/shoppingCart/productName")
    fun getShoppingCartByProductPartialName(@RequestParam("name") name: String,
                                            @RequestParam("page") page: Int,
                                            @RequestParam("pageSize") pageSize: Int): ResponseEntity<Any> {
        val output = shoppingCartService.getShoppingCartsByProductPartialNameWithPage(name, page, pageSize)
        return ResponseEntity.ok(PageShoppingCartByProductNameDto(totalPages = output.totalPages,
                totalElements = output.totalElements,
                selectedProducts = MapperUtil.INSTANCE.mapShoppingCartDto(output.content)))
    }

    @PostMapping("/shoppingCart/add")
    fun addShoppingCart(@RequestBody shoppingCart: AddShoppingCartDto): ResponseEntity<Any> {
        val output = shoppingCartService.save(shoppingCart)

        val outputDto = MapperUtil.INSTANCE.mapShoppingCartDto(output)
        outputDto?.let{ return ResponseEntity.ok(it)}
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
    }

//    @GetMapping("/shoppingCart/customerNamePageable")
//    fun getShoppingCartByCustomerNameWithpage(@RequestParam("name") name: String,
//                                              @RequestParam("page") page: Int,
//                                              @RequestParam("pageSize") pageSize: Int): ResponseEntity<Any> {
//        val output = shoppingCartService.getShoppingCartsByCustomerNameWithPage(name, page, pageSize)
//        return ResponseEntity.ok(PageShoppingCartDto(totalPages = output.totalPages,
//                totalElements = output.totalElements,
//                selectedProducts = MapperUtil.INSTANCE.mapShoppingCartByCustomerDto(output.content)))
//    }
}