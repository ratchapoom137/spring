package dv.spring.kotlin.repository

import dv.spring.kotlin.entity.Customer
import dv.spring.kotlin.entity.User
import dv.spring.kotlin.entity.UserStatus
import dv.spring.kotlin.entity.dto.CustomerDto
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.repository.CrudRepository

interface CustomerRepository: CrudRepository<Customer,Long>{
    fun findByName(name: String): Customer
    fun findByNameEndingWith(name: String): List<Customer>
    fun findByNameContainingIgnoreCaseOrEmailContainingIgnoreCase(name: String, email: String): List<Customer>
    fun findByDefaultAddress_ProvinceContainingIgnoreCase(name: String): List<Customer>
    fun findByUserStatus(status: UserStatus): List<Customer>
    fun findByUserStatus(status: UserStatus, page: Pageable): Page<Customer>
    fun findByIsDeletedIsFalse():List<Customer>
}